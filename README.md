# Mister Anderson
[![wercker status](https://app.wercker.com/status/656dcc14e3291f093d9bb08ff4d77394/s/master "wercker status")](https://app.wercker.com/project/bykey/656dcc14e3291f093d9bb08ff4d77394)

Surprised to see me?

There are quite a few packages around that already give you some tools to interact with a Neo4j instance, so why build
yet another one? Primarily: to gain a better insight in node.js, JavaScript, npm and not in the least Neo4j.
Secondarily: I personally found the existing solutions a bit wieldy to work with (probably my NIH-syndrome) and there's
room for a more complete Neo4j + node.js package.

## Installation
    npm install mister-anderson

## Current status
This module is currently under heavy development and as such hasn't got much to offer yet!
 
TODO: write more in the readme
