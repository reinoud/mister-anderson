# Features
_18-02-2014 List adapted from http://docs.neo4j.org/chunked/2.0.1/rest-api.html_


[ ] Not implemented (yet)  
[x] Implemented and tested  
[~] Implemented but not explicitly tested (should work though)  
[g] Implemented and available in Object Graph Mapper  
[-] Will not implement/not an implementation chapter  


## 19.1 Transactional HTTP Endpoint

[ ] 19.1.1. Begin a transaction  
[ ] 19.1.2. Execute statements in an open transaction  
[ ] 19.1.3. Execute statements in an open transaction in REST format for the return  
[ ] 19.1.4. Reset transaction timeout of an open transaction  
[ ] 19.1.5. Commit an open transaction  
[ ] 19.1.6. Rollback an open transaction  
[ ] 19.1.7. Begin and commit a transaction in one request  
[ ] 19.1.8. Return results in graph format  
[ ] 19.1.9. Handling errors  

## 19.2. Neo4j Status Codes
[ ] 19.2.1. Classifications  
[ ] 19.2.2. Status codes  

## 19.3. Service root
[x] 19.3.1. Get service root  

## 19.4. Streaming

[x] 19.4 Streaming  ***(Default & non-optional)***  

## 19.5. Cypher queries via REST
[x] 19.5.1. Use parameters  
[x] 19.5.2. Create a node  
[x] 19.5.3. Create a node with multiple properties  
[x] 19.5.4. Create mutiple nodes with properties  
[~] 19.5.5. Set all properties on a node using Cypher  
[~] 19.5.6. Send a query  
[~] 19.5.7. Return paths  
[~] 19.5.8. Nested results  
[~] 19.5.9. Retrieve query metadata  
[ ] 19.5.10. Errors  

## 19.6. Property values
[-] 19.6.1. Arrays  
[-] 19.6.2. Property keys  
[x] 19.6.3. List all property keys  

## 19.7. Nodes
[x] 19.7.1. Create node  
[x] 19.7.2. Create node with properties  
[ ] 19.7.3. Get node  
[x] 19.7.4. Get non-existent node  
[ ] 19.7.5. Delete node  
[ ] 19.7.6. Nodes with relationships cannot be deleted  

## 19.8. Relationships
[ ] 19.8.1. Get Relationship by ID  
[ ] 19.8.2. Create relationship  
[ ] 19.8.3. Create a relationship with properties  
[ ] 19.8.4. Delete relationship  
[ ] 19.8.5. Get all properties on a relationship  
[ ] 19.8.6. Set all properties on a relationship  
[ ] 19.8.7. Get single property on a relationship  
[ ] 19.8.8. Set single property on a relationship  
[ ] 19.8.9. Get all relationships  
[ ] 19.8.10. Get incoming relationships  
[ ] 19.8.11. Get outgoing relationships  
[ ] 19.8.12. Get typed relationships  
[ ] 19.8.13. Get relationships on a node without relationships  

## 19.9. Relationship types
[ ] 19.9.1. Get relationship types  

## 19.10. Node properties
[ ] 9.10.1. Set property on node  
[ ] 19.10.2. Update node properties  
[ ] 19.10.3. Get properties for node  
[ ] 19.10.4. Property values can not be null  
[ ] 19.10.5. Property values can not be nested  
[ ] 19.10.6. Delete all properties from node  
[ ] 19.10.7. Delete a named property from a node  

## 19.11. Relationship properties
[ ] 19.11.1. Update relationship properties  
[ ] 19.11.2. Remove properties from a relationship  
[ ] 19.11.3. Remove property from a relationship  
[ ] 19.11.4. Remove non-existent property from a relationship  
[ ] 19.11.5. Remove properties from a non-existing relationship  
[ ] 19.11.6. Remove property from a non-existing relationship  

## 19.12. Node labels
[ ] 19.12.1. Adding a label to a node  
[ ] 19.12.2. Adding multiple labels to a node  
[ ] 19.12.3. Adding a label with an invalid name  
[ ] 19.12.4. Replacing labels on a node  
[ ] 19.12.5. Removing a label from a node  
[ ] 19.12.6. Listing labels for a node  
[ ] 19.12.7. Get all nodes with a label  
[ ] 19.12.8. Get nodes by label and property  
[ ] 19.12.9. List all labels  

## 19.13. Indexing
[ ] 19.13.1. Create index  
[ ] 19.13.2. List indexes for a label  
[ ] 19.13.3. Drop index  

## 19.14. Constraints
[ ] 19.14.1. Create uniqueness constraint  
[ ] 19.14.2. Get a specific uniqueness constraint  
[ ] 19.14.3. Get all uniqueness constraints for a label  
[ ] 19.14.4. Get all constraints for a label  
[ ] 19.14.5. Get all constraints  
[ ] 19.14.6. Drop constraint  

## 19.15. Traversals
[ ] 19.15.1. Traversal using a return filter  
[ ] 19.15.2. Return relationships from a traversal  
[ ] 19.15.3. Return paths from a traversal  
[ ] 19.15.4. Traversal returning nodes below a certain depth  
[ ] 19.15.5. Creating a paged traverser  
[ ] 19.15.6. Paging through the results of a paged traverser  
[ ] 19.15.7. Paged traverser page size  
[ ] 19.15.8. Paged traverser timeout  

## 19.16. Graph Algorithms
[ ] 19.16.1. Find all shortest paths  
[ ] 19.16.2. Find one of the shortest paths  
[ ] 19.16.3. Execute a Dijkstra algorithm and get a single path  
[ ] 19.16.4. Execute a Dijkstra algorithm with equal weights on relationships  
[ ] 19.16.5. Execute a Dijkstra algorithm and get multiple paths  


## 19.17. Batch operations
[ ] 19.17.1. Execute multiple operations in batch  
[ ] 19.17.2. Refer to items created earlier in the same batch job  
[ ] 19.17.3. Execute multiple operations in batch streaming  

## 19.18. Legacy indexing
[ ] 19.18.1. Create node index  
[ ] 19.18.2. Create node index with configuration  
[ ] 19.18.3. Delete node index  
[ ] 19.18.4. List node indexes  
[ ] 19.18.5. Add node to index  
[ ] 19.18.6. Remove all entries with a given node from an index  
[ ] 19.18.7. Remove all entries with a given node and key from an index  
[ ] 19.18.8. Remove all entries with a given node, key and value from an index  
[ ] 19.18.9. Find node by exact match  
[ ] 19.18.10. Find node by query  

## 19.19. Unique Indexing
[ ] 19.19.1. Get or create unique node (create)  
[ ] 19.19.2. Get or create unique node (existing)  
[ ] 19.19.3. Create a unique node or return fail (create)  
[ ] 19.19.4. Create a unique node or return fail (fail)  
[ ] 19.19.5. Add an existing node to unique index (not indexed)  
[ ] 19.19.6. Add an existing node to unique index (already indexed)  
[ ] 19.19.7. Get or create unique relationship (create)  
[ ] 19.19.8. Get or create unique relationship (existing)  
[ ] 19.19.9. Create a unique relationship or return fail (create)  
[ ] 19.19.10. Create a unique relationship or return fail (fail)  
[ ] 19.19.11. Add an existing relationship to a unique index (not indexed)  
[ ] 19.19.12. Add an existing relationship to a unique index (already indexed)  

## 19.20. Legacy Automatic Indexes
[ ] 19.20.1. Find node by exact match from an automatic index  
[ ] 19.20.2. Find node by query from an automatic index  

## 19.21. Configurable Legacy Automatic Indexing
[ ] 19.21.1. Create an auto index for nodes with specific configuration  
[ ] 19.21.2. Create an auto index for relationships with specific configuration  
[ ] 19.21.3. Get current status for autoindexing on nodes  
[ ] 19.21.4. Enable node autoindexing  
[ ] 19.21.5. Lookup list of properties being autoindexed  
[ ] 19.21.6. Add a property for autoindexing on nodes  
[ ] 19.21.7. Remove a property for autoindexing on nodes  