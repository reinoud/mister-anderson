# OGM
The main benefit of using Mister Anderson is that you'll have a full flexed OGM at your proposal.

## OGM
```javascript 
    var graph = new neo.graph(config); 
    var ogm = new neo.ogm(graph);
    var trinity = new neo.node({ name: 'Trinity'});
    var smith = new neo.node({ name: 'Agent Smith'});
    trinity.relates.to(smith).with({ affection: 'hostile' });
    
    // returns trinity
    console.log(smith.relates.from());
    
    // should smith be persisted too?
    ogm.persist(trinity);
    
```