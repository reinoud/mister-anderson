#Roadmap

The goal of Mr. Anderson is to provide an easy but robust way to work with Neo4j (and not only expose the RESTful API
through JavaScript). This roadmap is intended to streamline the development by grouping small chunks of functionality,
starting with basic support for storing Nodes and Relationships and ending with a full fledged Object Graph Mapper.
 
This OGM should handle features like batch operations, constraints and indexing without the need to explicitly program
this in an end-user application.

##0.1 Basics CRUD
This version should be able to create node and relationship objects and persist them in Neo4j. Fetched data from Neo4j
should be mapped to node and relationship objects.
 
- base node object with crud methods 
- base relationship object with crud methods
- map response to above objects

##0.2 Cypher
In version 0.2 the mapping of response to nodes and relationships will be improved to support cypher queries.

- map results cypher query to node and relationship objects

##0.3 OGM beginnings
In this phase the OGM will have it's pillars build. Extra care should be taken in the design of the OGM. It should be
possible to extend the OGM with support for Neo4j extension and maybe even other database systems (via an adapter).

The OGM could turn out to be a separate module from Mr. Anderson.

##0.? OGM Batch
Implement automatic batching. -> node1.save(); node2.save(); ogm.save();

##0.? OGM Structures
Easy to implement common graph structures like r-tree etc.

##0.? OGM Cache

##2.? Harmony Rewrite?
Native support for promises is nice.