"use strict";
var request = require('request');

//TODO: Implement uri caching to store HATEOES discovered urls

/**
 * Wrapp function to handle API Requests and Neo4j errors
 * @param options
 * @param callback
 * @constructor
 */
var ApiRequest = function (options, callback) {
  options.json = true;
  request(options, function(error, response, body) {
    if (error) {
      callback(error);
    }
    var statusCode;
    switch(statusCode = response.statusCode) {
      case 200:
      case 201:
        callback(null, body);
        break;
      default:
        callback(new Error("Server return statuscode " + statusCode), null);
    }
  });
};

module.exports = ApiRequest;