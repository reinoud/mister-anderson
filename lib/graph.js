"use strict";

var request = require('./api-request');
var url = require('url');

/**
 * Creates a new database object which sits in front of a Neo4j REST API. Set init to true and pass along a callback
 * function to setup the service root of the database using discovery.
 * @param {object} options
 * @param {function} [callback] - This callback will passed to the Graph.init() function.
 * @constructor
 */
var Graph = function (options, callback) {
  options = options || {};
  this.initialized = false;
  this.host = null;

  if ('string' !== typeof options.host && 'object' !== typeof options.host) {
    callback(new Error('You must specify a Graph host. See the documentation or source for the right syntax.'));
  }

  this.setHost(options.host);

  if ('function' === typeof callback) {
    this.init(null, callback);
  }
};

/**
 * Initialize the Graph database. Use this function to (re)set the service root.
 * @param {object} options
 * @param {function} callback
 */
Graph.prototype.init = function (options, callback) {
  var self, params;
  self = this;
  params = {};
  options = options || {};

  if (options.host) {
    this.setHost(options.host);
  }

  params.uri = this.host;
  params.method = 'GET';

  if ('function' !== typeof callback) {
    callback(new Error('The Graph.init() function expects a callback function.'));
  }

  request(params, function (err, jsonData) {
    if (err) {
      callback(err);
    }

    self.root = jsonData;
    self.initialized = true;
    callback(null, self.root);
  });
};

/**
 * Set the url (and port) of the Neo4j REST API
 * @param {string|object} location - Either a string or JSON encoded url. A url string should not have a trailing slash.
 */
Graph.prototype.setHost = function (location) {
  this.host = ('string' === typeof location) ? url.parse(location) : location;
  this.host.path = '/db/data/';
};

/**
 * List all the property keys ever used in the database (including deleted ones).
 * @todo Discover the path variable instead of assuming the documented value (currently a Neo4j REST API limitation)
 * @param callback
 */
Graph.prototype.listProperties = function(callback) {
  var options = {};

  options.uri = this.host;
  options.uri.path += 'propertykeys';
  options.method = 'GET';
  options.json = true;

  request(options, callback);
};

Graph.prototype.node = function (data, callback) {
  var options = {};
  var Node = require('node');

  options.uri = url.parse(this.root.node);
  options.json = true;

  // TODO get uri for specific node via hateoas
  if ('number' === typeof data) {
    options.method = 'GET';
    options.uri.path += data;
  } else {
    options.method = 'POST';
    options.body = data;
  }

  request(options, function(err, node) {
    callback(err, node);
  });
};

Graph.prototype.node_index = function () {
  var NodeIndex;
  NodeIndex = require('./node-index');
  return new NodeIndex(this.root.node_index);
};

Graph.prototype.relationship_index = function () {
  var RelationshipIndex;
  RelationshipIndex = require('./relationship-index');
  return new RelationshipIndex(this.root.relationship_index);
};

Graph.prototype.extensions_info = function () {
  var ExtensionsInfo;
  ExtensionsInfo = require('./extensions-info');
  return new ExtensionsInfo(this.root.extensions_info);
};

Graph.prototype.relationship_types = function () {
  var RelationshipTypes;
  RelationshipTypes = require('./relationship-types');
  return new RelationshipTypes(this.root.relationship_types);
};

Graph.prototype.batch = function () {
  var Batch;
  Batch = require('batch');
  return new Batch(this.root.batch);
};

/**
 *
 * @param {string|object} query
 * @param {function} callback
 */
Graph.prototype.cypher = function (query, callback) {
  var options = {};

  // objectify query if provided as string
  if ('string' === typeof query) {
    query = { query: query };
  }

  options.uri = url.parse(this.root.cypher);
  options.method = 'POST';
  options.body = JSON.stringify(query);

  request(options, callback);
};

Graph.prototype.transaction = function () {
  var Transaction;
  Transaction = require('./transaction');
  return new Transaction(this.root.transaction);
};

Graph.prototype.neo4j_version = function () {
  return this.root.neo4j_version;
};

module.exports = Graph;