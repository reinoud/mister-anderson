"use strict";

var Node = function () {
  var self = this;
  this.save = function () {
    return new Node.Promise(function (resolve) {
      self.persisted = true;
      resolve();
    });
  };
};

module.exports = function (Promise) {
  Node.Promise = Promise;
  return Node;
};