"use strict";
var neo, host;

neo = require('../lib/');
host = 'http://localhost:7474';

exports.cypher = {
  setUp: function (callback) {
    this.graph = new neo.graph({host: host}, function () {
      callback();
    });
  },

  simpleQuery: function (test) {
    var query = "CREATE (n:TestPerson {name: 'NEO' }) RETURN n";
    this.graph.cypher(
      query,
      function (err, response) {
        test.strictEqual(err, null, "The error object should be null.");
        test.equal(response.columns[0], "n", "The response should return n as it's first column listing");
        test.strictEqual(response.data.length, 1, "There should only be one node created and returned under 'n'");
        test.done();
      }
    );
  },

  parameters: function (test) {
    var query, params;

    query = {
      query: "CREATE (n:TestPerson {name: {name} }) RETURN n",
      params: { name: "Neo"}
    };

    this.graph.cypher(
      query,
      function (err, response) {
        test.strictEqual(err, null, "The error object should be null.");
        test.equal(response.columns[0], "n", "The response should return n as it's first column listing");
        test.strictEqual(response.data.length, 1, "There should only be one node created and returned under 'n'");
        test.equal(response.data[0][0].data.name, "Neo", "The name property of the node should be 'Neo'");
        test.done();
      }
    );
  },

  multipleProperties: function (test) {
    var query;
    query = {
      query: "CREATE (n:TestPerson { props }) RETURN n",
      params: {
        "props": {
          "name": "Neo",
          "nickname": "Mister Anderson"
        }
      }
    };

    this.graph.cypher(
      query,
      function (err, response) {
          test.strictEqual(err, null, "The error object should be null.");
          test.equal(response.columns[0], "n", "The response should return n as it's first column listing");
          test.strictEqual(response.data[0].length, 1, "There should only be one node created and returned under 'n'");
          test.equal(response.data[0][0].data.nickname, "Mister Anderson", "The nickname property of the node should be 'Mister Anderson'");
          test.done();
      }
    );
  },

  multiplePropertiesMultipleNodes: function (test) {
    var query;

    query = {
      query: "CREATE (n:TestPerson { props }) RETURN n",
      params: {
        "props": [
          {
            "name": "Neo",
            "nickname": "Mister Anderson"
          },
          {
            "name": "Agent"
          }
        ]
      }
    };

    this.graph.cypher(
      query,
      function (err, response) {
        test.strictEqual(err, null, "The error object should be null.");
        test.equal(response.columns[0], "n", "The response should return n as it's first column listing");
        test.strictEqual(response.data.length, 2, "There should only be two nodes created and returned under 'n'");
        test.equal(response.data[1][0].data.name, "Agent", "The name property of the 2nd node should be 'Agent'");
        test.done();
      }
    );
  },

  tearDown: function (callback) {
    var query;

    query = "MATCH (n:TestPerson) OPTIONAL MATCH (n)-[r]-() DELETE r,n";

    this.graph.cypher(query, function () {
      callback();
    });
  }
};