"use strict";
var neo4j = require('../lib/');
var host = 'http://localhost:7474';

exports.init = {

    setUp: function (callback) {
        this.rootLinks = [
            'batch',
            'cypher',
            'extensions',
            'extensions_info',
            'neo4j_version',
            'node',
            'node_index',
            'relationship_index',
            'relationship_types',
            'transaction'
        ];
        callback();
    },

    // call init manually
    explicit: function (test) {
        var graph;
        var self = this;

        test.throws(function () {
            graph = new neo4j.graph();
        }, "Creating a new graph without specifying the host should throw an error");

        // a new database should not be initialized yet
        graph = new neo4j.graph({host: host});
        test.strictEqual(graph.initialized, false);
        graph.init(
            null,
            function (err, root) {
                test.strictEqual(err, null, "The error should be null");
                self.rootLinks.forEach(function (link) {
                    test.notEqual(typeof root[link], 'undefined', 'The root-link must exist');
                });
                test.equal(graph.initialized, true);
                test.done();
            }
        );
    },

    // call init automatically by providing a callback to the contructor
    implicit: function (test) {
        var graph;
        var self = this;

        graph = new neo4j.graph(
            {host: host, streaming: true },
            function (err, root) {
                test.strictEqual(err, null, "The error should be null");
                self.rootLinks.forEach(function (link) {
                    test.notStrictEqual(typeof root[link], 'undefined', 'The root-link must exist');
                });
                test.equal(graph.initialized, true);
                test.done();
            }
        );
    }
};