"use strict";
// we need to insert a promise library to offer user their own choice of A+ library
var Promise = require("bluebird");

exports.node = function (test) {
  var node = require('../lib/node')(Promise);

  var neo = Object.create(null, {
    name: { value: "Neo" }
  });
  node.call(neo);

  test.ok(!neo.persisted, "The node should not be persisted yet.");

  neo.save().then(function () {
    test.ok(neo.persisted, "The state of the node should say 'persisted'.");
    test.done();
  });

  var trinity = Object.create(node, {
    name: { value: "Trinity" }
  });

  test.ok(!trinity.persisted, "The node should not be persisted.");

};