"use strict";

var Ogm = require('../lib/ogm');
var Node = require('../lib/node');

exports.ogm = function (test) {
  var ogm = new Ogm();

  var john = new Node();
  ogm.persist(john);

  var jane = new Node({name: "Jane"});
  ogm.persist(jane);

  jane.relate('KNOWS', john);

  john.set({name: "John"});

  ogm.save();

  test.done();
};