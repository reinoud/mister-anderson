"use strict";

var neo, host;

neo = require('../lib/');
host = 'http://localhost:7474';

exports.PropertyValues = {
  setUp: function (callback) {
    this.graph = new neo.graph({host: host}, function () {
      callback();
    });
  },

  listProperties: function (test) {
    this.graph.listProperties(function(err, response) {
      test.strictEqual(err, null, "The error object should be null.");
      test.ok(response instanceof Array, "The response data should be an array.");
      test.done();
    });
  }
};
