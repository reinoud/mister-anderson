"use strict";
var neo, async, host;

neo = require('../../lib/');
async = require('async');
host = 'http://localhost:7474';


async.waterfall([
  function(callback) {
    var graph;
    graph = new neo.graph({host: host}, function () {
      callback(null, graph);
    });
  },
  function(graph, callback) {
    var query;

    query = "MATCH (n) OPTIONAL MATCH (n)-[r]-() DELETE r,n";

    graph.cypher(query, function () {
      callback(null, 'done');
    });
  }
],
  function(err, result) {
    console.log(result);
  }
);


